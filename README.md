# [deltaprocessing.ru](https://deltaprocessing.ru) source codes

<br/>

### Run deltaprocessing.ru on localhost

    $ sudo vi /etc/systemd/system/deltaprocessing.ru.service

Insert code from deltaprocessing.ru.service

    $ sudo systemctl enable deltaprocessing.ru.service
    $ sudo systemctl start deltaprocessing.ru.service
    $ sudo systemctl status deltaprocessing.ru.service

http://localhost:4072
